<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\ParkedCar;
use App\Models\Parking;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ParkingCarController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index(Parking $parking) {
        $cars = $parking->parkedCars()->with('car', 'parking')->get();
        return response()->json($cars);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * Entry in parking
     */
    public function store(Request $request, Parking $parking) {

        $request->validate([
            'model' => ['required', 'min:3'],
            'model_type' => ['nullable', 'min:3']
        ]);

        $car = Car::firstOrCreate($request->all());

        ParkedCar::create([
            'car_id' => $car->id,
            'parking_id' => $parking->id
        ]);


        return response()->json([
            "messsage" => 'Car has been parked',
            "parking" => $parking,
            "car" => $car
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Parking $parking, string $id) {

        $parked_car = ParkedCar::where([
            'parking_id' => $parking->id,
            'car_id' => $id
        ])->whereNull('exit')->first();

        if($parked_car) {
            $parked_car->update([
                'exit' => Carbon::now()
            ]);
        } else {
            return response()->json([
                "message" => "Car was not found in parking"
            ], 404);
        }
        return response()->json($parked_car);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
