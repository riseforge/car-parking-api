<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Parking;
use Illuminate\Http\Request;

class ParkingController extends Controller {

    public function index() {
        $parkings = Parking::all();
        return response()->json($parkings);
    }

    public function store(Request $request) {
        $request->validate([
            'name' => ['required', 'min:4'],
            'capacity' => ['required', 'integer', 'min:10']
        ]);
        $new_parking = Parking::create($request->all());
        return response()->json($new_parking);
    }

    public function update(Request $request, Parking $parking) {
        $request->validate([
            'name' => ['nullable', 'min:4'],
            'capacity' => ['nullable', 'integer', 'min:10']
        ]);

        $parking->update($request->all());
        return response()->json($parking);
    }

}
