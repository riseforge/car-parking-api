<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model {
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'model',
        'model_type',
        'license_plate',
        'chasis_number',
    ];




}
