<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkedCar extends Model {
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'car_id',
        'parking_id',
        'entry',
        'exit',
    ];

    public function car(): BelongsTo {
        return $this->belongsTo(Car::class);
    }

    public function parking(): BelongsTo {
        return $this->belongsTo(Parking::class);
    }
}
