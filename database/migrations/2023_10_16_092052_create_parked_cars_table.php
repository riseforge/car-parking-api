<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('parked_cars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('car_id');
            $table->foreignId('parking_id');
            $table->timestamp('entry')->useCurrent();
            $table->timestamp('exit')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
            $table->foreign('parking_id')->references('id')->on('parkings')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parked_cars');
    }
};
