<?php

namespace Database\Seeders;

use App\Models\Parking;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParkingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void {
        Parking::create([
            'name' => 'Arnold parking',
            'capacity' => 27,
        ]);

        Parking::create([
            'name' => 'Amr parking',
            'capacity' => 50,
        ]);

        Parking::create([
            'name' => 'Jul parking',
            'capacity' => 75,
        ]);
    }
}
