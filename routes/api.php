<?php

use App\Http\Controllers\Api\CarController;
use App\Http\Controllers\Api\ParkingCarController;
use App\Http\Controllers\Api\ParkingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('car', [CarController::class, 'index'])->name('car.index');

// Route::get('parking', [ParkingController::class, 'index'])->name('parking.index');
// Route::post('parking', [ParkingController::class, 'store'])->name('parking.store');
// Route::put('parking/{parking}', [ParkingController::class, 'update'])->name('parking.update');

Route::resource('parking', ParkingController::class)->only(['index', 'store', 'update']);
Route::resource('parking.car', ParkingCarController::class);
